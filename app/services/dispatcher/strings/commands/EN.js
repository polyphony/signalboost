module.exports = {
  ADD: ['ADD'],
  ACCEPT: ['ACCEPT'],
  DECLINE: ['DECLINE'],
  HELP: ['HELP'],
  INFO: ['INFO'],
  INVITE: ['INVITE'],
  JOIN: ['HELLO', 'JOIN'], // we recognize "JOIN" and "LEAVE" for backwards compatibility
  LEAVE: ['GOODBYE', 'LEAVE'],
  REMOVE: ['REMOVE'],
  RENAME: ['RENAME'],
  RESPONSES_ON: ['RESPONSES ON'],
  RESPONSES_OFF: ['RESPONSES OFF'],
  VOUCHING_ON: ['VOUCHING ON'],
  VOUCHING_OFF: ['VOUCHING OFF'],
  SET_LANGUAGE: ['ENGLISH', 'INGLÉS', 'INGLES', 'ANGLAIS'],
}
